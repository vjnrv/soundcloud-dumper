const fs = require('fs')
const async = require('async')
const https = require('https')
const rimraf = require('rimraf')
const request = require('request')
const progress = require('request-progress')
const _cliProgress = require('cli-progress')

/**
 * Classe auxiliar para fazer dump de contas do SoundCloud.
 */
class Dumper {
  /**
   * Constructor.
   * 
   * @param {SoundCloud} soundcloud 
   */
  constructor (soundcloud) {
    this.soundcloud = soundcloud
    this.dumpDir = './public/dump'
    this.tracksDir = `${this.dumpDir}/tracks`
    this.playlistsDir = `${this.dumpDir}/playlists`
    this.counter = 0
    this.total = 0
    this.progress = new _cliProgress.Bar({
      format: '{title} [{bar}] {percentage}% | ETA: {eta}s | {value}/{total} | Speed: {speed} MB/s'
    }, _cliProgress.Presets.shades_classic)
  }

  /**
   * Inicia o Dump de uma Conta.
   * 
   * @param {int} soundCloudUserId 
   */
  async dump (soundCloudUserId) {
    return this._getPlaylists(soundCloudUserId)

    // Prepara o diretoriokbit
    rimraf.sync(this.dumpDir)
    fs.mkdirSync(this.dumpDir)
    fs.mkdirSync(this.tracksDir)
    fs.mkdirSync(this.playlistsDir)

    // Passo 1 - Salva os arquivos
    this.soundcloud.getTracksByUserId(soundCloudUserId, 2000)
      .then(data => {
        const tracks = data.collection
        this.total = tracks.length
        this.counter = 0
        this.progress.start(this.total, 0)

        async.eachLimit(tracks, 1, (track, callback) => {
          track.cli_title = `${track.title} (${this.counter}/${this.total})`
          this._dumpTrack(track, this.tracksDir)
            .then(() => callback())
            .catch(err => { throw err })
        }, () => {
          this.progress.stop()
          this._getPlaylists(soundCloudUserId)
        }) // Finish...
      })
      .catch(err => { throw err })
  }

  _getPlaylists (soundCloudUserId) {
    //   Passo 2 - Salva as playlists e faz a organização
    this.soundcloud.getPlaylistsByUserId(soundCloudUserId, 2000)
      .then(data => {
        const playlists = data.collection
        this.total = playlists.length
        this.counter = 0
        this.progress.start(this.total, 0)

        async.eachLimit(playlists, 1, (playlist, callback) => {
          const playlistDir = `${this.playlistsDir}/${playlist.permalink}`

          if (!fs.existsSync(playlistDir)) {
            fs.mkdirSync(playlistDir)
          }

          // Salva as informações em JSON
          fs.writeFileSync(`${playlistDir}/playlist.json`, JSON.stringify(playlist))
          // Salva resumo em TXT
          fs.writeFileSync(`${playlistDir}/playlist.txt`, [
            `Titulo: ${playlist.title}`,
            `Permalink: ${playlist.permalink_url}`,
            `Data: ${playlist.created_at}`,
            `Tags: ${playlist.tag_list}`,
            `Genero: ${playlist.genre}`,
            `Faixas: ${playlist.tracks.length}`,
            `Curtidas: ${playlist.likes_count}`
          ].join("\r\n"))

          // Salva a imagem da faixa
          if (playlist.artwork_url) {
            progress(request(playlist.artwork_url.replace('large', 'original')))
              .pipe(fs.createWriteStream(`${playlistDir}/playlist.jpg`))
          }

          playlist.cli_title = `${playlist.title} (${this.counter}/${this.total})`

          playlist.tracks.forEach((track, index) => {
            if (!track.title) {
              // Track incompleta, pegar JSON da pasta.
              track = JSON.parse(fs.readFileSync(`${this.tracksDir}/${track.id}.json`))
            }

            const number = index + 1
            const trackDir = `${playlistDir}/${number}.${track.permalink}`

            if (!fs.existsSync(trackDir)) {
              fs.mkdirSync(trackDir)
            }

            fs.copyFileSync(`${this.tracksDir}/${track.id}.mp3`, `${trackDir}/${track.permalink}.mp3`)
            fs.copyFileSync(`${this.tracksDir}/${track.id}.json`, `${trackDir}/${track.permalink}.json`)

            // Salva resumo em TXT
            fs.writeFileSync(`${trackDir}/${track.permalink}.txt`, [
              `Playlist: ${playlist.title}`,
              `Titulo: ${track.title}`,
              `Permalink: ${track.permalink_url}`,
              `Data: ${track.created_at}`,
              `Tags: ${track.tag_list}`,
              `Genero: ${track.genre}`,
              `Curtidas: ${track.likes_count}`
            ].join("\r\n"))

            // Imagem
            if (fs.existsSync(`${this.tracksDir}/${track.id}.jpg`)) {
              fs.copyFileSync(`${this.tracksDir}/${track.id}.jpg`, `${trackDir}/${track.permalink}.jpg`)
            }
          })

          this.counter++
          this.progress.update(this.counter, {
            title: playlist.cli_title,
            speed: 0
          })

          callback()
        }, () => this.progress.stop()) // Finish...
      })
      .catch(err => { throw err })

  }

  /**
   * Faz o dump de uma playlist.
   * 
   * @param {Object} playlist 
   */
  _dumpPlaylist (playlist) {
    return new Promise((resolve, reject) => {
      const playlistDir = `${this.dumpDir}/${playlist.title}`
      let number = 1
      let total = playlist.tracks.length
      fs.mkdirSync(playlistDir)

      async.eachLimit(playlist.tracks, 1, (track, callback) => {
        const trackDir = `${playlistDir}/${number}. ${track.title}`
        track.playlist = playlist.title
        track.cli_title = `${playlist.title} (${number}/${total})`

        this._dumpTrack(track, trackDir, number++)
          .then(() => callback())
          .catch(err => reject(err))
      }, () => resolve())
    })
  }

  /**
   * Faz o dump de uma faixa.
   * 
   * @param {Object} track Objeto da Faixa
   * @param {String} trackDir Pasta de Destino
   */
  _dumpTrack (track, trackDir) {
    return new Promise((resolve, reject) => {
      if (!fs.existsSync(trackDir)) {
        fs.mkdirSync(trackDir)
      }

      // Salva as informações em JSON
      fs.writeFileSync(`${trackDir}/${track.id}.json`, JSON.stringify(track))

      // Salva a imagem da faixa
      if (track.artwork_url) {
        progress(request(track.artwork_url.replace('large', 'original')))
          .pipe(fs.createWriteStream(`${trackDir}/${track.id}.jpg`))
      }

      this.progress.update(this.counter, {
        title: track.cli_title
      })

      this._download(track, trackDir)
        .then(() => resolve())
        .catch(err => reject(err))
    })
  }

  _download (track, trackDir) {
    const input = this.soundcloud.getTrackDownloadUrl(track)
    const output = `${trackDir}/${track.id}.mp3`

    return new Promise((resolve, reject) => {
      progress(request(input))
      .on('progress', state => {
        this.progress.update(this.counter, {
          title: track.cli_title,
          speed: (state.speed / 1024/ 1024).toFixed(2),
          eta: state.time.remaining
        })
      })
      .on('error', err => reject(err))
      .on('end', () => {
        this.counter++
        this.progress.update(this.counter, {
          title: track.cli_title
        })
        resolve()
      })
      .pipe(fs.createWriteStream(output))
    })
  }
}

module.exports = Dumper