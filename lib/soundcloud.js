
const axios = require('axios')

class SoundCloud {
  constructor (clientId) {
    this.clientId = clientId
    this.endpoint = 'https://api-v2.soundcloud.com'
    this.client = axios.create({
      baseURL: this.endpoint,
      // https://stackoverflow.com/questions/46288437/set-cookie-header-has-no-effect-about-using-cookies-cors-including-for-local/46412839#46412839
      withCredentials: true,
      headers: {}
    })
  }

  async getTracksByUserId (userId, limit = 10) {
    const res = await this.client.get(`/users/${userId}/tracks`, {
      params: {
        limit: limit,
        client_id: this.clientId
      }
    })

    return res.data
  }

  async getPlaylistsByUserId (userId, limit = 10) {
    const res = await this.client.get(`/users/${userId}/playlists`, {
      params: {
        limit: limit,
        client_id: this.clientId
      }
    })

    return res.data
  }

  /**
   * Retorna a URL de download de uma faixa.
   * 
   * @param {Object} track
   * @returns {String}
   */
  getTrackDownloadUrl (track) {
    if (!track.downloadable) {
      return `https://api.soundcloud.com/tracks/${track.id}/stream?client_id=${this.clientId}`
    }

    return `${track.download_url}?client_id=${this.clientId}`
  }
}

module.exports = SoundCloud